<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Аккаунт';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Проверить';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';